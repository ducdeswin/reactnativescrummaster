import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button } from 'react-native-elements'

import StartButton from "./src/components/StartButton";
import Timer from "./src/components/Timer";
import TimerInput from "./src/components/TimerInput";
import StateCount from "./src/components/StateCount";

export default class App extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            isActive: false,
            devs: 0,
            seconds: '00',
            minutes: '15',
            devsDone: 1,
            bgWidth: '100',
            timePerDev: '15',
            showInputText: true,
            showTimer: false,
            showText: false
        };

        this.secondsRemaining;
        this.intervalHandle;
        this.handleDevCount = this.handleDevCount.bind(this);

// method that triggers the countdown functionality
        this.startCountDown = this.startCountDown.bind(this);
        this.stopCountDown = this.stopCountDown.bind(this);
        this.goToInput = this.goToInput.bind(this);
        this.goToTimer = this.goToTimer.bind(this);
        this.goToText = this.goToText.bind(this);
        this.tick = this.tick.bind(this);
    }

    handleDevCount(devCount) {
        const timePerDev = this.state.minutes / parseInt(devCount);

        this.setState({
            devs: devCount ? parseInt(devCount) : '0',
            timePerDev: timePerDev ? parseInt(timePerDev) : 0,
            seconds: '00',
        })

    }

    tick() {
        let min = Math.floor(this.secondsRemaining / 60);
        let sec = this.secondsRemaining - (min * 60);
        let totalTimeInSec = this.state.timePerDev * 60;
        let bgWidthSize = 100 * this.secondsRemaining / totalTimeInSec;

        this.setState({
            minutes: min,
            seconds: sec,
            bgWidth: bgWidthSize
        });

        if (sec < 10) { this.setState({ seconds: "0" + this.state.seconds }); }
        if (min < 10) { this.setState({ value: "0" + min }); }
        if (min === 0 && sec === 0) {
            clearInterval(this.intervalHandle);
            this.setState({
                devsDone: this.state.devsDone++
            });
            this.restartTimer();
        }

        this.secondsRemaining--
    }

    restartTimer() {
        clearInterval(this.intervalHandle);

        if ( this.state.devsDone <= this.state.devs ) {
            this.intervalHandle = setInterval(this.tick, 1000);
        }
    }

    startCountDown() {
        if (this.state.isActive) {return;}

        this.setState({isActive: true});
        this.intervalHandle = setInterval(this.tick, 1000);
        let time = this.state.timePerDev;
        this.secondsRemaining = time * 60;
    }

    goToTimer() {
        this.startCountDown();
            this.setState({
                isActive: false,
                showInputText: false,
                showTimer: true,
                showText: false
    });
    }

    goToText() {
        if (0 === this.state.devs) {
            return;
        }
        this.setState({
            isActive: false,
            showInputText: false,
            showTimer: false,
            showText: true
        })
    }

    goToInput() {
        clearInterval(this.intervalHandle);

        this.setState({
            isActive: false,
            showInputText: true,
            showTimer: false,
            showText: false,
            bgWidth: '100',
        })
    }

    stopCountDown() {
        clearInterval(this.intervalHandle);
        this.setState({
            isActive: false,
            seconds: '00',
            minutes: '0'
        });
    }

    renderInput() {
        return <View style={styles.timerInput}>
            <TimerInput minutes={this.state.timePerDev} icon={'done'}
                        goToTimer={this.goToText} parent={this} /></View>;
    }

    renderText() {
        return <View style={styles.inputText}>
            <Text style={styles.textAfterInput}>Chaque personne a droit à {this.state.timePerDev} minutes ...</Text>
            <Button
                containerViewStyle={{borderRadius:50, marginTop:10}}
                buttonStyle={{borderRadius:50}}
                onPress={this.goToTimer}
                icon={{name: 'cached'}}
                title='Lancer le timer' />
            <Button
                containerViewStyle={{borderRadius:50, marginTop:10}}
                buttonStyle={{borderRadius:50}}
                onPress={this.goToInput}
                // icon={{name: 'arrow_back'}}
                title='Retour' />
        </View>;
    }

    renderTimer () {
        return <View>
            <StateCount done={this.state.devsDone} total={this.state.devs}/>
            <Timer seconds={this.state.seconds} minutes={this.state.minutes}/>

            <View>
                <StartButton startCountDown={this.startCountDown} icon={'check'}/>
                <StartButton startCountDown={this.stopCountDown} icon={'stop'}/>
            </View>

            <View>
                <StartButton startCountDown={this.goToInput} icon={'create'}/>
            </View>
        </View>;
    }
    render() {
        let contentRender;
        if (this.state.showInputText) {
            contentRender = this.renderInput()
        } else if (this.state.showText) {
            contentRender = this.renderText()
        } else {
            contentRender = this.renderTimer()
        }

        return (
            <View style={styles.container}>
                <View style={[styles.timerBG, {width: this.state.bgWidth + '%'}]}/>
                <View style={styles.content}>
                    {contentRender}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    inputText: {
        marginBottom: 20,
    },
    textAfterInput: {
        fontSize: 50,
        paddingLeft: 30,
        paddingRight: 30,
        paddingBottom: 100,
        color: '#fff'
    },
    timerBG: {
        zIndex: 1 ,
        backgroundColor: '#e73948',
        height: '100%',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0
    },
    timerInput: {
        width: '80%'
    },
    container: {
        position: 'relative',
        flex: 1,
        backgroundColor: '#343434',
    },
    content: {
        zIndex:2,
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    }
});
