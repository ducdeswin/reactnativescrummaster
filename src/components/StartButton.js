import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button } from 'react-native-elements'

export default class StartButton extends React.Component {
    render() {
        return (
            <Button
                containerViewStyle={{borderRadius:50, marginTop:10}}
                buttonStyle={{borderRadius:50}}
                icon={{name: this.props.icon}}
                onPress={this.props.startCountDown}
            />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#343434',
    },
});
