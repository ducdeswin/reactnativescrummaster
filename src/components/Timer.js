import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class Timer extends React.Component {
    render() {
        return (
            <Text style={styles.bigText}>{this.props.minutes}:{this.props.seconds}</Text>
        );
    }
}

const styles = StyleSheet.create({
    bigText: {
        fontSize: 100,
        color: "#fff",
        fontWeight: 'bold'
}
});
