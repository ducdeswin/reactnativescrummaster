import React from 'react';
import { StyleSheet, TextInput,View } from 'react-native';
import { Button } from 'react-native-elements'

export default class TimerInput extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <TextInput
                    style={styles.input}
                    keyboardType='numeric'
                    placeholder="Combien de devs?"
                    maxLength={6}
                    minLength={2}

                    required
                    onChangeText={(text) => this.props.parent.handleDevCount(text)}
                />

                <Button
                    style={styles.button}
                    containerViewStyle={{borderRadius:50, marginTop:10}}
                    buttonStyle={{borderRadius:50}}
                    // icon={{name: this.props.icon}}
                    backgroundColor={'#ffffff'}
                    color={'#000000'}
                    title='Next'
                    onPress={this.props.goToTimer}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
    },
    button: {
        backgroundColor: '#fff'
    },
    input: {
        height: 50,
        paddingBottom: 10,
        borderBottomColor: '#fff',
        width: '90%',
        fontSize: 20,
        color: "#000",
        fontWeight: 'bold',
        marginBottom: 100
    }
});
