import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button } from 'react-native-elements'

export default class StateCount extends React.Component {
    render() {
        return (
            <Text style={styles.text}>{this.props.done} / {this.props.total}</Text>
        );
    }
}

const styles = StyleSheet.create({
    text: {
        textAlign: 'center',
        fontSize: 20,
        color: '#fff',
    },
});
